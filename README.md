# Codeberg Mail To Issue Service

This is a simple POC for a service, that receives e-mails and creates issues on our Gitea instance.

The actual tool is developed here: <https://codeberg.org/Codeberg-Infrastructure/email-service>
