use std::time::Duration;

use chrono::{DateTime, Utc};
use reqwest::{
    header::{HeaderMap, ACCEPT, AUTHORIZATION},
    Client,
};
use serde::{Deserialize, Serialize};

use crate::{
    config::Gitea,
    error::{GiteaClientError, Result},
};

pub struct GiteaClient {
    base_url: String,
    repo_name: String,
    req_client: Client,
}

#[derive(Debug, Serialize)]
struct IssuePostBody<'a> {
    title: &'a str,
    body: &'a str,
}

#[derive(Debug, Serialize)]
struct IssueCommentPostBody<'a> {
    body: &'a str,
}

#[derive(Debug, Deserialize)]
struct Issue {
    number: u32,
}

#[derive(Debug, Deserialize)]
pub struct IssueComment {
    pub id: u32,
    pub html_url: String,
    pub issue_url: String,
    pub body: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub user: User,
}

#[derive(Debug, Deserialize)]
pub struct User {
    pub id: u32,
    pub username: String,
    pub full_name: String,
}

impl GiteaClient {
    pub fn new(config: Gitea) -> GiteaClient {
        let mut header_map = HeaderMap::new();
        header_map.insert(
            AUTHORIZATION,
            format!("Bearer {}", config.token).parse().unwrap(),
        );
        header_map.insert(ACCEPT, "application/json".parse().unwrap());

        GiteaClient {
            base_url: config.base_url.trim_end_matches('/').to_string(),
            repo_name: config.repo_name.to_string(),
            req_client: Client::builder()
                .connect_timeout(Duration::from_secs(5))
                .user_agent(format!("cmtis v{}", env!("CARGO_PKG_VERSION")))
                .default_headers(header_map)
                .build()
                .expect("could not build reqwest client"),
        }
    }

    pub async fn create_issue(&self, title: &str, body: &str) -> Result<u32> {
        let url = format!("{}/api/v1/repos/{}/issues", self.base_url, self.repo_name);

        match self
            .req_client
            .post(url)
            .json(&IssuePostBody { title, body })
            .send()
            .await
        {
            Ok(res) => match res.status().as_u16() {
                200..=299 => {
                    log::debug!("created issue");
                    let issue: Issue = res.json().await?;

                    Ok(issue.number)
                }
                code => {
                    log::warn!("unexpected status code when creating issue: {code}");
                    log::debug!("response: {:?}", res.text().await);

                    Err(GiteaClientError::UnsuccessfulRequest(code))
                }
            },
            Err(why) => {
                log::error!("failed to create issue: {why:?}");

                Err(GiteaClientError::ReqwestError(why))
            }
        }
    }

    #[allow(dead_code)]
    pub async fn create_issue_comment(&self, id: u32, body: &str) -> Result<()> {
        let url = format!(
            "{}/api/v1/repos/{}/issues/{id}/comments",
            self.base_url, self.repo_name
        );

        match self
            .req_client
            .post(url)
            .json(&IssueCommentPostBody { body })
            .send()
            .await
        {
            Ok(res) => match res.status().as_u16() {
                200..=299 => {
                    log::debug!("created issue comment");

                    Ok(())
                }
                code => {
                    log::warn!("unexpected status code when creating issue comment: {code}");
                    log::debug!("response: {:?}", res.text().await);

                    Err(GiteaClientError::UnsuccessfulRequest(code))
                }
            },
            Err(why) => {
                log::error!("failed to create issue comment: {why:?}");

                Err(GiteaClientError::ReqwestError(why))
            }
        }
    }

    #[allow(dead_code)]
    pub async fn get_issue_comments(&self) -> Result<Vec<IssueComment>> {
        let url = format!(
            "{}/api/v1/repos/{}/issues/comments",
            self.base_url, self.repo_name
        );

        match self.req_client.get(url).send().await {
            Ok(res) => match res.status().as_u16() {
                200..=299 => {
                    log::debug!("received issue comments");

                    Ok(res.json().await?)
                }
                code => {
                    log::warn!("unexpected status code when receiving issue comments: {code}");
                    log::debug!("response: {:?}", res.text().await);

                    Err(GiteaClientError::UnsuccessfulRequest(code))
                }
            },
            Err(why) => {
                log::error!("failed to receive issue comments: {why:?}");

                Err(GiteaClientError::ReqwestError(why))
            }
        }
    }
}
