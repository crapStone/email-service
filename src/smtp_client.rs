use async_imap::{error::Result, extensions::idle::IdleResponse::*, types::Fetch, Session};
use async_native_tls::TlsStream;
use async_std::{net::TcpStream, stream::StreamExt};
use tokio::sync::mpsc::{self, Receiver, Sender};

use crate::config::Smtp;

pub struct SMTPClient {
    session: Session<TlsStream<TcpStream>>,
}

impl SMTPClient {
    pub async fn new(config: &Smtp) -> Result<SMTPClient> {
        let tls = async_native_tls::TlsConnector::new();

        #[cfg(debug_assertions)]
        let tls = tls
            .danger_accept_invalid_certs(true)
            .danger_accept_invalid_hostnames(true);

        let client =
            async_imap::connect((config.domain.as_str(), 3993), &config.domain, tls).await?;

        let mut imap_session = client
            .login(&config.login, &config.password)
            .await
            .map_err(|e| e.0)?;
        imap_session.select(&config.mailbox).await?;

        Ok(SMTPClient {
            session: imap_session,
        })
    }

    async fn message_loop(self, send: Sender<Result<Fetch>>) -> Result<()> {
        let mut session = self.session;
        loop {
            let messages = session.uid_search("UNSEEN").await?;

            // just concatenate all ids to be able to receive the unseen mails
            let uid_set: String = messages
                .into_iter()
                .map(|uid| {
                    let mut s = uid.to_string();
                    s.push(',');

                    s
                })
                .collect();

            {
                let mut messages_stream = session
                    .uid_fetch(uid_set.trim_end_matches(','), "RFC822")
                    .await?;

                while let Some(m) = messages_stream.next().await {
                    send.send(m).await.unwrap();
                }
            }

            let mut idle = session.idle();
            idle.init().await?;
            let (idle_wait, _interrupt) = idle.wait();

            if let ManualInterrupt = idle_wait.await? {
                session = idle.done().await?;
                session.logout().await?;
                break;
            }

            session = idle.done().await?;
        }

        Ok(())
    }

    pub async fn start_message_receive_loop(self) -> Receiver<Result<Fetch>> {
        let (send, recv) = mpsc::channel(1);

        tokio::spawn(async move {
            self.message_loop(send).await.unwrap();
        });

        recv
    }
}
