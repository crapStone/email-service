use async_imap::{error::Result, types::Fetch};
use mailparse::*;
use tokio::sync::mpsc::Receiver;

use crate::gitea_client::GiteaClient;

pub async fn mail_to_issue(mut message_stream: Receiver<Result<Fetch>>, gitea_client: GiteaClient) {
    while let Some(m) = message_stream.recv().await {
        let m2 = m.unwrap();
        let body = m2.body().unwrap();

        let mail = parse_mail(body).unwrap();

        let title = mail.headers.get_first_value("Subject").unwrap();
        let body = mail.get_body().unwrap();

        let issue_id = gitea_client.create_issue(title.trim(), body.trim()).await;
        match issue_id {
            Ok(id) => log::debug!("created issue with id: {id}"),
            Err(why) => log::error!("failed to create issue: '{why:?}'"),
        }

        // TODO(#4) save issue number and mail address in database
    }
}
