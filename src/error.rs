use thiserror::Error;

#[derive(Error, Debug)]
pub enum GiteaClientError {
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),

    #[error("Unsuccessful response code: {0}")]
    UnsuccessfulRequest(u16),
}

pub type Result<T> = std::result::Result<T, GiteaClientError>;
