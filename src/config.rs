use std::{env, fs};

use serde::Deserialize;

use crate::flags::CLIArgs;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub smtp: SmtpConfig,
    pub gitea: GiteaConfig,

    pub runtime: Option<RuntimeConfig>,
}

#[derive(Debug, Deserialize)]
pub struct SmtpConfig {
    pub domain: Option<String>,
    pub login: Option<String>,
    pub password: Option<String>,
    pub mailbox: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct GiteaConfig {
    pub base_url: Option<String>,
    pub repo_name: Option<String>,
    pub token: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct RuntimeConfig {
    pub threads: Option<u8>,
}

pub struct CombinedConfig {
    pub smtp: Smtp,
    pub gitea: Gitea,

    pub runtime: Runtime,
}

pub struct Smtp {
    pub domain: String,
    pub login: String,
    pub password: String,
    pub mailbox: String,
}

pub struct Gitea {
    pub base_url: String,
    pub repo_name: String,
    pub token: String,
}

pub struct Runtime {
    pub threads: Option<u8>,
}

pub fn read_config(cli: &CLIArgs) -> Config {
    let contents = fs::read(
        cli.config
            .clone()
            .unwrap_or_else(|| env::current_exe().unwrap().join("config.toml")),
    )
    .expect("could not read config file");
    toml::from_slice(&contents).expect("could not deserialize config file")
}

pub fn merge(cli: CLIArgs, config: Config) -> CombinedConfig {
    CombinedConfig {
        smtp: Smtp {
            domain: combine(cli.domain, config.smtp.domain, "mail domain"),
            login: combine(cli.login, config.smtp.login, "mail login/username"),
            password: combine(cli.password, config.smtp.password, "mail password"),
            mailbox: combine(cli.mailbox, config.smtp.mailbox, "mailbox"),
        },
        gitea: Gitea {
            base_url: combine(cli.gitea_url, config.gitea.base_url, "gitea base url"),
            repo_name: combine(cli.repo_name, config.gitea.repo_name, "gitea repo name"),
            token: combine(cli.token, config.gitea.token, "gitea token"),
        },
        runtime: Runtime {
            threads: cli
                .threads
                .or_else(|| config.runtime.and_then(|runtime| runtime.threads)),
        },
    }
}

fn combine<T>(opta: Option<T>, optb: Option<T>, name: &str) -> T {
    opta.or(optb).unwrap_or_else(|| {
        panic!("no '{name}' set! please set it using the environment, cli flags or config.")
    })
}
