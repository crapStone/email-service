mod config;
mod error;
mod flags;
mod gitea_client;
mod glue;
mod smtp_client;

use clap::StructOpt;
use config::CombinedConfig;
use flags::CLIArgs;
use gitea_client::GiteaClient;
use smtp_client::SMTPClient;

async fn async_main(config: CombinedConfig) {
    let smtp_client = SMTPClient::new(&config.smtp)
        .await
        .expect("could not create smtp-client");

    let message_stream = smtp_client.start_message_receive_loop().await;

    let gitea_client = GiteaClient::new(config.gitea);

    glue::mail_to_issue(message_stream, gitea_client).await;
}

fn main() {
    std::env::set_var("ASYNC_STD_THREAD_COUNT", "1");
    pretty_env_logger::init();

    let args = CLIArgs::parse();

    let config = config::read_config(&args);

    let config = config::merge(args, config);

    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .worker_threads(config.runtime.threads.unwrap_or(4).into())
        .build()
        .expect("failed to create runtime");

    runtime.block_on(async_main(config));
}
