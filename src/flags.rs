use std::path::PathBuf;

use clap::Parser;

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
/// "Codeberg mail to issue service".
/// This daemon listens for e mails and creates issues in a predefined repository on a Gitea instance.
pub struct CLIArgs {
    #[clap(short, long, env = "CMTIS_MAIL_DOMAIN")]
    /// domain of the mail server
    pub domain: Option<String>,
    #[clap(short, long, env = "CMTIS_MAIL_LOGIN")]
    /// login/username of the mail server
    pub login: Option<String>,
    #[clap(short, long, env = "CMTIS_MAIL_PASSWORD")]
    /// password of the mail server
    pub password: Option<String>,
    #[clap(short, long, env = "CMTIS_MAILBOX")]
    /// mailbox on the mail server
    pub mailbox: Option<String>,
    #[clap(short, long, env = "CMTIS_GITEA_URL")]
    /// base url of the gitea server
    pub gitea_url: Option<String>,
    #[clap(short, long, env = "CMTIS_REPO_NAME")]
    /// repo name where issues should be created
    pub repo_name: Option<String>,
    #[clap(short, long, env = "CMTIS_GITEA_TOKEN")]
    /// token for the gitea server
    pub token: Option<String>,
    #[clap(long, env = "CMTIS_THREADS")]
    /// number of threads the runtime should use
    pub threads: Option<u8>,

    #[clap(short, long, env = "CMTIS_CONFIG_FILE")]
    /// path of config file
    pub config: Option<PathBuf>,
}
